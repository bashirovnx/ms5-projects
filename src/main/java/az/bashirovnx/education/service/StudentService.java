package az.bashirovnx.education.service;

import az.bashirovnx.education.dto.StudentDto;
import az.bashirovnx.education.entity.StudentEntity;
import az.bashirovnx.education.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static az.bashirovnx.education.mapper.StudentMapper.INSTANCE;


@Service
@RequiredArgsConstructor
@Slf4j
public class StudentService {

    private final StudentRepository studentRepository;

    public ResponseEntity<Page<StudentDto>> getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);

        Page<StudentEntity> entities = studentRepository.findAll(pageable);

        Page<StudentDto> response = entities.map(INSTANCE::toStudentDto);

        return ResponseEntity.ok(response);
    }

    public ResponseEntity<StudentDto> getStudentById(Long id) {
        return ResponseEntity.ok(INSTANCE.toStudentDto(studentRepository.findById(id)
                .orElseThrow(IllegalArgumentException::new)));
    }

    public StudentDto createStudent(StudentDto studentDto) {

        return INSTANCE.toStudentDto(studentRepository
                .save(INSTANCE.toStudentEntity(studentDto)));
    }

    public StudentDto updateStudent(Long id, StudentDto studentDto) {
        Optional<StudentEntity> entity = studentRepository.findById(id);

        if (entity.isPresent()) {
            log.info("Student found in DB. Update Student by ID:" + id);
            return INSTANCE.toStudentDto(studentRepository.save(INSTANCE.toStudentEntity(studentDto, id)));
        } else {
            log.info("Student not found in DB. Create student");
            return createStudent(studentDto);
        }
    }

    public void deleteStudent(Long id) {
        studentRepository.deleteById(id);
    }

}
