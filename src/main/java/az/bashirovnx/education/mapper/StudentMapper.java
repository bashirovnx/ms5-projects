package az.bashirovnx.education.mapper;

import az.bashirovnx.education.dto.StudentDto;
import az.bashirovnx.education.entity.StudentEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface StudentMapper {

    StudentMapper INSTANCE = Mappers.getMapper(StudentMapper.class);

    @Mapping(source = "birthdate", target = "dateOfBirth")
    StudentEntity toStudentEntity(StudentDto studentDto);

    @Mapping(source = "dateOfBirth", target = "birthdate")
    StudentDto toStudentDto(StudentEntity studentEntity);

    @Mapping(source = "studentDto.birthdate", target = "dateOfBirth")
    @Mapping(source = "studentId", target = "id")
    StudentEntity toStudentEntity(StudentDto studentDto, Long studentId);

}
